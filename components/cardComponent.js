import StarIcon from "@material-ui/icons/Star";
import CheckCircleIcon from "@material-ui/icons/CheckCircle";
import AccessTimeIcon from "@material-ui/icons/AccessTime";
import AcUnitIcon from "@material-ui/icons/AcUnit";
import RadioButtonUncheckedOutlinedIcon from "@material-ui/icons/RadioButtonUncheckedOutlined";
function cardComponent() {
  return (
    <div className="col-md-8 col-12">
      <div className="mt-3 cardBorder">
        <div>
          <span className="tag-grey">Cloud Engineering</span>
        </div>
        <br />
        <div
          style={{
            display: "flex",
            justifyContent: "space-between",
            alignItems: "center",
          }}
        >
          <h3>Start Azure demo project</h3>

          <div>
            <span>
              <StarIcon style={{ color: "blue" }} />
            </span>
            <span className="blueText">20,000</span>
          </div>
        </div>
        <div>
          Microsoft Azure has decided to partner with us and supply us with a
          company wide support for our engineer with active support. Begin your
          journey to upskill yourself to be an Azure cloud engineer.
        </div>

        <div className="py-3">
          <ul>
            <li>
              <CheckCircleIcon
                style={{ color: "blue", backgroundColor: "white" }}
              />
              <span></span>
              <div style={{ flexBasis: "65%", paddingLeft:'10px' }}>
                Sign up for Microsoft Azure Cloud
              </div>

              <div style={{ marginLeft: "auto", order: 2 }}>
                <button type="button" className="btn btn-light btn-sm">
                  <span
                    style={{ color: "#b5bbc1" }}
                    className="font-weight-bold"
                  >
                    Done
                  </span>
                </button>
              </div>
            </li>
            <li>
              <CheckCircleIcon
                style={{ color: "blue", backgroundColor: "white" }}
              />
              <span></span>
              <div style={{ flexBasis: "65%", paddingLeft:'10px' }}>
                Complete weekly security patches
              </div>
              <div style={{ marginLeft: "auto", order: 2 }}>
                <button type="button" className="btn btn-light btn-sm">
                  <span
                    style={{ color: "#b5bbc1" }}
                    className="font-weight-bold"
                  >
                    Done
                  </span>
                </button>
              </div>
            </li>
            <li>
              <CheckCircleIcon
                style={{ color: "blue", backgroundColor: "white" }}
              />
              <span></span>
              <div style={{ flexBasis: "65%", paddingLeft:'10px' }}>
                Complete the updating of security patches for your customer and
                help them stay secure from cyber attacks.
              </div>
              <div style={{ marginLeft: "auto", order: 2 }}>
                <button type="button" className="btn btn-light btn-sm">
                  <span
                    style={{ color: "#b5bbc1" }}
                    className="font-weight-bold"
                  >
                    Done
                  </span>
                </button>
              </div>
            </li>

            <li>
              <RadioButtonUncheckedOutlinedIcon
                style={{ color: "#E8F1FF", backgroundColor: "white" }}
              />
              <span></span>
              <div style={{ flexBasis: "65%", paddingLeft:'10px' }}>
                Launch project in dev enviroment and send link to complete
              </div>
              <div style={{ marginLeft: "auto", order: 2 }}>
                <button
                  type="button"
                  style={{ backgroundColor: "#E8F1FF" }}
                  className="btn btn-light btn-sm"
                >
                  <span
                    style={{ color: "#0062ff" }}
                    className="font-weight-bold"
                  >
                    Start
                  </span>
                </button>
              </div>
            </li>
          </ul>
        </div>
        <hr />

        <div className="row">
          <div className="col-md-4 d-flex align-items-center">
            <div>
              <AcUnitIcon
                style={{
                  color: "#3dd598",
                  backgroundColor: "#e2f9f0",
                  fontSize: "30px",
                  padding: "3px",
                  borderRadius: "25px",
                }}
              />
            </div>
            <div>
              <div>Cyber Defender</div>
              <div className="greyText">Badge</div>
            </div>
          </div>

          <div className="col-md-4 d-flex align-items-center">
            <div>
              <AccessTimeIcon
                style={{
                  color: "#ff4f4f",
                  backgroundColor: "#ffe6e6",
                  fontSize: "30px",
                  padding: "3px",
                  borderRadius: "25px",
                }}
              />
            </div>
            <div className="pl-2">
              <div>2 days</div>
              <div className="greyText">Time Left</div>
            </div>
          </div>

          <div className="col-md-4 d-flex align-items-center">
            <div className="d-flex">
              <img
                style={{ zIndex: 4, border: "3px solid #FFFFFF" }}
                className="rounded-circle"
                width="30px"
                height="30px"
                src="/image1.png"
              />
              <img
                style={{
                  zIndex: 3,
                  position: "absolute",
                  left: "30px",
                  border: "3px solid #FFFFFF",
                }}
                className="rounded-circle"
                width="30px"
                height="30px"
                src="/image2.png"
              />
              <img
                style={{ zIndex: 2, border: "3px solid #FFFFFF" }}
                className="rounded-circle"
                width="30px"
                height="30px"
                src="/image3.png"
              />
              <img
                style={{
                  zIndex: 1,
                  position: "absolute",
                  left: "60px",
                  border: "3px solid #FFFFFF",
                }}
                className="rounded-circle"
                width="30px"
                height="30px"
                src="/image4.png"
              />
            </div>
            <div style={{ position: "absolute", left: "90px" }}>
              <div>4 People</div>
              <div className="greyText">Moderator</div>
            </div>
          </div>
        </div>

        <hr />

        <div>Created 12 January: 2019</div>
      </div>

      <div className="mt-3 cardBorder">
        <div
          style={{
            display: "flex",
            justifyContent: "space-between",
            alignItems: "center",
          }}
        >
          <h3>Attend Company Annual Dinner 2020</h3>

          <div>
            <span>
              <StarIcon style={{ color: "blue" }} />
            </span>
            <span className="blueText">20,000</span>
          </div>
        </div>
        <div className="d-flex justify-content-between">
          <div style={{ flexBasis: "65%" }}>
            Our company 10th annual dinner is happening this Friday. Please mark
            your attandance for the dinner.
          </div>
          <div>
            <button type="button" className="btn btn-primary btn-sm">
              <span className="font-weight-bold">Start</span>
            </button>
          </div>
        </div>

        <hr />

        <div className="row">
          <div className="col-md-4 d-flex align-items-center">
            <div>
              <AcUnitIcon
                style={{
                  color: "#3dd598",
                  backgroundColor: "#e2f9f0",
                  fontSize: "30px",
                  padding: "3px",
                  borderRadius: "25px",
                }}
              />
            </div>
            <div>
              <div>Loyalty Dinner</div>
              <div className="greyText">Badge</div>
            </div>
          </div>

          <div className="col-md-4 d-flex align-items-center">
            <div>
              <AccessTimeIcon
                style={{
                  color: "#ff4f4f",
                  backgroundColor: "#ffe6e6",
                  fontSize: "30px",
                  padding: "3px",
                  borderRadius: "25px",
                }}
              />
            </div>
            <div className="pl-2">
              <div>5 days</div>
              <div className="greyText">Time Left</div>
            </div>
          </div>
        </div>

        <hr />

        <div className="d-flex align-items-center">
          <AccessTimeIcon
            style={{
              color: "#92929d",
              fontSize: "30px",
              padding: "3px",
              borderRadius: "25px",
            }}
          />
          12 January 2019
          <div style={{ marginLeft: "auto" }}>
            <a href="#">Submit Quest ></a>
          </div>
        </div>
      </div>
    </div>
  );
}

export default cardComponent;
