import React, { useState, useCallback } from "react";

function subHeader() {
  const [state, setState] = useState({
    id: "",
  });
  const setActiveClass = (e, id) => {
    e.preventDefault();
    setState({ id: id });
  };
  return (
    <div className="py-3">
      <span
        onClick={(e) => setActiveClass(e, 1)}
        id="1"
        className={state.id == 1 ? "mainTab active" : "mainTab"}
      >
        All
        <span
          id="tag"
          className={state.id == 1 ? "tag-blue" : "tag-blue-not-active"}
        >
          2
        </span>
      </span>

      <span
        onClick={(e) => setActiveClass(e, 2)}
        id="2"
        className={state.id == 2 ? "mainTab active" : "mainTab"}
      >
        In Review
        <span
          id="tag"
          className={state.id == 2 ? "tag-blue" : "tag-blue-not-active"}
        >
          2
        </span>
      </span>
      <span
        onClick={(e) => setActiveClass(e, 3)}
        id="3"
        className={state.id == 3 ? "mainTab active" : "mainTab"}
      >
        Complete
        <span
          id="tag"
          className={state.id == 3 ? "tag-blue" : "tag-blue-not-active"}
        >
          2
        </span>
      </span>
      <span
        onClick={(e) => setActiveClass(e, 4)}
        id="4"
        className={state.id == 4 ? "mainTab active" : "mainTab"}
      >
        Moderator
        <span
          id="tag"
          className={state.id == 4 ? "tag-blue" : "tag-blue-not-active"}
        >
          2
        </span>
      </span>
      <span
        onClick={(e) => setActiveClass(e, 5)}
        id="5"
        className={state.id == 5 ? "mainTab active" : "mainTab"}
      >
        Admin
        <span
          id="tag"
          className={state.id == 5 ? "tag-blue" : "tag-blue-not-active"}
        >
          2
        </span>
      </span>
    </div>
  );
}

export default subHeader;
