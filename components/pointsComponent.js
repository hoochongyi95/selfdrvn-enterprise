import AcUnitIcon from "@material-ui/icons/AcUnit";

import SchoolOutlinedIcon from "@material-ui/icons/SchoolOutlined";
function pointsComponent() {
  return (
    <div className="col-md-4">
      <div className="cardBorder">
        <div>
          <span className="greyText">Points</span>
        </div>
        <div className="player">
          <div className="player-icon"></div>
          <div className="progressLoading">
            <div className="right-side"></div>
            <div className="left-side"></div>
          </div>
          <div className="player-text">
            <div>My Points</div>
            <span className="font-weight-bold">785,823</span>
            <div className="greyText">/1000,000</div>
          </div>
        </div>

        <div>
          <span className="greyText">Skills</span>
        </div>

        <div className="d-flex">
          <SchoolOutlinedIcon
            style={{
              color: "#0062ff",
              backgroundColor: "#E8F1FF",
              fontSize: "30px",
              padding: "3px",
              borderRadius: "25px",
            }}
          />
          <div className="px-3">
            <div>Agile Methoddology</div>
            <div>
              <span className="greyText">17 relevant questions</span>
            </div>
          </div>
        </div>

        <div className="d-flex py-2">
          <SchoolOutlinedIcon
            style={{
              color: "#0062ff",
              backgroundColor: "#E8F1FF",
              fontSize: "30px",
              padding: "3px",
              borderRadius: "25px",
            }}
          />
          <div className="px-3">
            <div>User Experience Analytics</div>
            <div>
              <span className="greyText">17 relevant questions</span>
            </div>
          </div>
        </div>

        <button
          className="col-md-12"
          type="button"
          className="btn btn-primary btn-block"
        >
          + Add Skills
        </button>
      </div>
    </div>
  );
}

export default pointsComponent;
