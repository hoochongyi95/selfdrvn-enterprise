import TextField from "@material-ui/core/TextField";
const directMain = () => {
  window.location.href = "/main";
};
function loginTemplate() {
  return (
    <div className="row h-100">
      <div className="col-md-6 mt-4">
        <div className="container col-md-8">
          <span style={{ color: "#0062ff" }}>Sign In</span>
          <h1 className="font-weight-bold">
            Next Generation Employee Engagement
          </h1>
          <div>
            Gigarena is a gig marketplace for DevOps work and a platform for
            companies to access a wide pool of talent that are constantly
            getting better through contineous upskilling through technical
            challenges & task.
          </div>
          <div className="pt-4">
            <label className="label-input">Username</label>
            <input
              style={{ paddingTop: "20px", paddingLeft: "8px", height: "50px" }}
              className="col-md-12"
            />
          </div>
          <div className="pt-4">
            <label className="label-input">Password</label>
            <input
              type="password"
              style={{ paddingTop: "20px", paddingLeft: "8px", height: "50px" }}
              className="col-md-12"
            />
          </div>
          <br />
          <div className="text-right py-2">
            <a href="#">Forgot Password ></a>
          </div>
          <button
            className="col-md-12"
            type="button"
            className="btn btn-primary btn-block"
          >
            Sign Up
          </button>

          <br />
          <div className="text-center">Or</div>

          <div class="space row">
            <div className="col-md-6 col-12">
              <button
                onClick={directMain}
                type="button"
                className="btn btn-outline-secondary btn-sm  btn-block"
              >
                <div>
                  <span>
                    <img width="30px" height="30px" src="/linkedinlogo.png" />
                  </span>
                  <span>Sign In With Linkedin</span>
                </div>
              </button>
            </div>
            <div className="col-md-6 col-12">
              <button
                type="button"
                onClick={directMain}
                className="btn btn-outline-secondary btn-sm  btn-block"
              >
                <span>
                  <img width="30px" height="30px" src="/googlelogo.jpg" />
                </span>
                Sign In With Google
              </button>{" "}
            </div>
          </div>
          <div className="text-right py-2">
            <a href="#">Dont Have an account? Register ></a>
          </div>
        </div>
      </div>
      <div className="col-md-6 img-background"></div>
    </div>
  );
}

export default loginTemplate;
