import SubHeader from "../components/subHeader";
import CardComponent from "../components/cardComponent";
import PointsComponent from "../components/pointsComponent";
import HomeOutlinedIcon from "@material-ui/icons/HomeOutlined";
import NotificationsNoneOutlinedIcon from "@material-ui/icons/NotificationsNoneOutlined";
import EmojiObjectsOutlinedIcon from "@material-ui/icons/EmojiObjectsOutlined";
import TrackChangesOutlinedIcon from "@material-ui/icons/TrackChangesOutlined";
import RedeemOutlinedIcon from "@material-ui/icons/RedeemOutlined";
import KeyboardArrowUpOutlinedIcon from "@material-ui/icons/KeyboardArrowUpOutlined";
import GroupWorkOutlinedIcon from "@material-ui/icons/GroupWorkOutlined";
import SportsEsportsOutlinedIcon from "@material-ui/icons/SportsEsportsOutlined";
import PeopleOutlineOutlinedIcon from "@material-ui/icons/PeopleOutlineOutlined";
import AssessmentOutlinedIcon from "@material-ui/icons/AssessmentOutlined";
import AddCircleOutlinedIcon from "@material-ui/icons/AddCircleOutlined";
import EventNoteOutlinedIcon from "@material-ui/icons/EventNoteOutlined";
import MoreHorizIcon from "@material-ui/icons/MoreHoriz";
import React, { useState } from "react";
let bool = false;
export default function MainPage() {
  const openDrawer = () => {
    bool = bool ? false : true;
    if (bool) {
      document.getElementById("mySidenav").style.width = "250px";
      document.getElementById("drawer").style.left = "250px";
    } else {
      document.getElementById("mySidenav").style.width = "0px";
      document.getElementById("drawer").style.left = "0px";
    }
  };
  return (
    <div className="mainContainer">
      <div className="mainAnnouncement">
        Starting from 18 March 2020, all employee of Nettium and Selfdrvn must
        work from home due to Covid-19
      </div>

      <div id="drawer" onClick={openDrawer}>
        &#9776;
      </div>

      <div id="mySidenav" className="sidenav">
        <div>
          <div className="d-flex px-4">
            <img src="/selfdrvnlogo.png" width="30px" height="30px" />
            <div
              className="px-2"
              style={{ color: "white", display: "flex", alignItems: "center" }}
            >
              SelfDrvn
            </div>
          </div>

          <div className="d-flex px-4 pt-4">
            <HomeOutlinedIcon
              style={{ color: "white", width: "30px", height: "30px" }}
            />
            <div
              className="px-2"
              style={{ color: "white", display: "flex", alignItems: "center" }}
            >
              Home
            </div>
          </div>

          <div className="d-flex px-4 pt-4">
            <NotificationsNoneOutlinedIcon
              style={{ color: "white", width: "30px", height: "30px" }}
            />
            <div
              className="px-2"
              style={{ color: "white", display: "flex", alignItems: "center" }}
            >
              Notifications
            </div>
            <span className="tag-white">1</span>
          </div>

          <div className="d-flex px-4 pt-4">
            <EmojiObjectsOutlinedIcon
              style={{ color: "white", width: "30px", height: "30px" }}
            />
            <div
              className="px-2"
              style={{ color: "white", display: "flex", alignItems: "center" }}
            >
              NewsFeed
            </div>
            <AddCircleOutlinedIcon
                           className="icon-main-style"

            />
          </div>

          <div className="d-flex px-4 pt-4">
            <EventNoteOutlinedIcon
              style={{ color: "white", width: "30px", height: "30px" }}
            />
            <div
              className="px-2"
              style={{ color: "white", display: "flex", alignItems: "center" }}
            >
              Quests
            </div>
            <AddCircleOutlinedIcon
              className="icon-main-style"
            />
          </div>

          <div className="d-flex px-4 pt-4">
            <TrackChangesOutlinedIcon
              style={{ color: "white", width: "30px", height: "30px" }}
            />
            <div
              className="px-2"
              style={{ color: "white", display: "flex", alignItems: "center" }}
            >
              Goal Setting
            </div>
            <AddCircleOutlinedIcon
                         className="icon-main-style"

            />
          </div>

          <div className="d-flex px-4 pt-4">
            <RedeemOutlinedIcon
              style={{ color: "white", width: "30px", height: "30px" }}
            />
            <div
              className="px-2"
              style={{ color: "white", display: "flex", alignItems: "center" }}
            >
              Rewards
            </div>
            <KeyboardArrowUpOutlinedIcon
                         className="icon-main-style"

            />
          </div>

          <div className="d-flex px-4 pt-4">
            <GroupWorkOutlinedIcon
              style={{ color: "white", width: "30px", height: "30px" }}
            />
            <div
              className="px-2"
              style={{ color: "white", display: "flex", alignItems: "center" }}
            >
              Collaborate
            </div>
            <KeyboardArrowUpOutlinedIcon
                         className="icon-main-style"

            />
          </div>

          <div className="d-flex px-4 pt-4">
            <SportsEsportsOutlinedIcon
              style={{ color: "white", width: "30px", height: "30px" }}
            />
            <div
              className="px-2"
              style={{ color: "white", display: "flex", alignItems: "center" }}
            >
              Games
            </div>
            <KeyboardArrowUpOutlinedIcon
                         className="icon-main-style"

            />
          </div>

          <div className="d-flex px-4 pt-4">
            <PeopleOutlineOutlinedIcon
              style={{ color: "white", width: "30px", height: "30px" }}
            />
            <div
              className="px-2"
              style={{ color: "white", display: "flex", alignItems: "center" }}
            >
              Organization
            </div>
            <KeyboardArrowUpOutlinedIcon
                        className="icon-main-style"

            />
          </div>

          <div className="d-flex px-4 pt-4">
            <AssessmentOutlinedIcon
              style={{ color: "white", width: "30px", height: "30px" }}
            />
            <div
              className="px-2"
              style={{ color: "white", display: "flex", alignItems: "center" }}
            >
              Reports
            </div>
            <KeyboardArrowUpOutlinedIcon
                          className="icon-main-style"

            />
          </div>

          <div
            style={{ paddingTop: "90px", paddingBottom: "20px" }}
            className="d-flex px-4 align-items-center"
          >
            <img
              src="/image1.png"
              style={{ borderRadius: "15px" }}
              width="30px"
              height="30px"
            />
            <div className="px-2" style={{ color: "white" }}>
              <div>Sudhan HH</div>
              <div style={{ fontSize: "12px" }}>admin</div>
            </div>
            <div style={{ marginLeft: "auto" }}>
              <MoreHorizIcon
                style={{
                  color: "white",
                  width: "30px",
                  height: "30px",
                }}
              />
            </div>
          </div>
        </div>
      </div>

      <SubHeader />
      <div
        style={{ backgroundColor: "#FFEFE4" }}
        className="cardBorder mx-4 d-flex"
      >
        <div style={{ flexBasis: "65%" }}>
          <h3 style={{ color: "#ff9649" }}>
            Advance Yourself. Earn Recognition Points. And Help Others Too.
          </h3>
          <div style={{ color: "#797979" }}>
            Meet your goals. Learn something new. Collaborate with others. This
            week there are{" "}
            <span style={{ color: "black" }} className="font-weight-bold">
              12
            </span>{" "}
            new quests available.
          </div>
        </div>
        <div style={{ position: "absolute", right: "120px", top: "40px" }}>
          <img src="/people.jpg" width="180px" height="210px" />
        </div>
      </div>
      <div className="d-flex">
        <CardComponent />
        <PointsComponent />
      </div>
    </div>
  );
}
